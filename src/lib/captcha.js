import React, { useState, useEffect } from "react";

export default function Captcha({ width, height, length, refreshkey, result }) {
  let svgTextTemp = [];
  const [svgText, setSvgText] = useState([]);
  let captchaText = "";
  const possibleChars =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  useEffect(() => {
    for (let i = 0; i < length; i++) {
      let thisChar = possibleChars.charAt(
        Math.floor(Math.random() * possibleChars.length)
      );
      svgTextTemp.push(`
      <text
        font-family="Verdana"
        font-size="16"
        x="${i*20+5}"
        y="16"
        fill="${"#" + (((1 << 24) * Math.random()) | 0).toString(16)}"
        transform="rotate(${Math.random() * (5 - 0) + 0})"
      >${thisChar}</text>
      `);
      captchaText = `${captchaText}${thisChar}`;
    }
    console.log("svgText", svgText.join("\n"));
    setSvgText(svgTextTemp);
    result(captchaText);
  }, [refreshkey]);

  return (
    <img
      src={
        "data:image/svg+xml;base64," +
        btoa(
          "<svg " +
            'xmlns="http://www.w3.org/2000/svg" ' +
            'view-box="0 0 ' +
            width +
            " " +
            height +
            '" ' +
            'height="' +
            height +
            '" ' +
            'width="' +
            width +
            '">' +
            svgText.join("") +
            "</svg>"
        )
      }
      alt=""
    />
  );
}

// this.text = []
// this.originText = []
// this.possible = ;
// for (var i = 0; i < this.state.lenght; i++) {
//     let char = this.possible.charAt(Math.floor(Math.random() * this.possible.length))
//     this.text.push(
//         `<text
//             font-family="${this.state.fontFamily}"
//             font-size="${this.state.fontSize}"
//             x="${this.state.paddingLeft * i}"
//             y="${this.state.paddingTop}"
//             fill="${ this.props.textColor ? this.props.textColor : "#" + ((1 << 24) * Math.random() | 0).toString(16)}"
//             transform="rotate(${Math.random() * (5 - 0) + 0})"
//         >${char}</text>`
//     )
//     this.originText.push(
//         char
//     )
// }
