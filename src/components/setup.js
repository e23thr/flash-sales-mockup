import React, { useState } from "react";

export default function Setup({ timebeforebuy, timeduringbuy, setconfig }) {
  const [tbb, setTbb] = useState(timebeforebuy);
  const [tdb, setTdb] = useState(timeduringbuy);
  // console.log('tbb',tbb, typeof tbb);
  return (
    <div className="columns">
      <div className="column is-half is-offset-one-quarter">
        <div className="content">
          <div className="field">
            <label className="label has-text-white">
              เวลานับถอยหลังก่อนเริ่ม
            </label>
            <p className="control has-icons-left has-icons-right">
              <input
                className="input"
                type="number"
                placeholder="เวลาก่อนเริ่ม"
                value={tbb}
                onChange={e => {
                  setTbb(e.target.value);
                }}
              />
              <span className="icon is-small is-left">
                <i className="fas fa-stopwatch" />
              </span>
              <span
                className={`icon is-small is-right ${
                  typeof isNaN(tbb) && tbb > 0 ? "has-text-info" : ""
                }`}
              >
                <i className="fas fa-check" />
              </span>
            </p>
          </div>
          <div className="field">
            <label className="label has-text-white">
              เวลาที่ใช้ได้ในการซื้อ
            </label>
            <p className="control has-icons-left has-icons-right">
              <input
                className="input"
                type="number"
                placeholder="เวลาที่ใช้ได้ในการซื้อ"
                value={tdb}
                onChange={e => {
                  setTdb(e.target.value);
                }}
              />
              <span className="icon is-small is-left">
                <i className="fas fa-stopwatch" />
              </span>
              <span
                className={`icon is-small is-right ${
                  typeof isNaN(tdb) && tdb > 0 ? "has-text-info" : ""
                }`}
              >
                <i className="fas fa-check" />
              </span>
            </p>
          </div>

          <fieldset disabled={isNaN(tbb) || isNaN(tdb) || tbb < 1 || tdb < 1}>
            <div className="control">
              <button
                className="button is-info"
                onClick={e => setconfig(tbb, tdb)}
              >
                เริ่มทำงาน
              </button>
            </div>
          </fieldset>
        </div>
      </div>
    </div>
  );
}
