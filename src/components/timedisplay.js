import React from "react";
import moment from "moment";

export default function TimeDisplay({ seconds, displaytype }) {
  function displayType1() {
    return (
      <>
        <span className="content">
          {moment(seconds).format("YYYY-MM-DD HH:mm:ss")}
        </span>
      </>
    );
  }

  function displayType2() {
    const m = moment(seconds);
    return (
      <>
        <span className="content">
          Starts in  {m.format("HH")}h {m.format("mm")}m {m.format("ss")}s
        </span>
      </>
    );
  }

  return displaytype === 1 ? <>{displayType1()}</> : <>{displayType2()}</>;
}
