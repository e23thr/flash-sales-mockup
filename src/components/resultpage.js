import React from "react";

export default function ResultPage({ text, className, count, startover }) {
  return (
    <div className={`hero ${className}`}>
      <div className="hero-body">
        <div className="container has-text-centered">
          <p className="title">You {text}</p>
          <p className="subtitle">
            your {text} result is {count}
          </p>
        </div>
      </div>
      <div className="hero-foot">
        <nav className="tabs is-fullwidth">
          <div className="container">
            <ul>
              <li>
                <a
                  href="#t"
                  onClick={e => {
                    e.preventDefault();
                    startover();
                  }}
                >
                  Start over
                </a>
              </li>
            </ul>
          </div>
        </nav>
      </div>
    </div>
  );
}
