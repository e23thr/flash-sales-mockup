import React, { useState } from "react";
import moment from "moment";
// import svgcaptcha from "svg-captcha";
// import RCG from "react-captcha-generator";
import Captcha from "../lib/captcha";
export default function Buypage({
  product,
  time,
  starttime,
  endtime,
  instock,
  setwincount,
  setlosecount
}) {
  // console.log(svgcaptcha.create().text);
  let hoursDiff = moment(starttime).diff(moment(time), "hours");
  let minutesDiff = moment(starttime).diff(moment(time), "minutes");
  let secondsDiff = moment(starttime).diff(moment(time), "seconds");
  if (secondsDiff < 0) {
    secondsDiff = 0;
    minutesDiff = 0;
    hoursDiff = 0;
  }

  const pageWait = "wait";
  const pageBuy = "buy";

  const [page, setPage] = useState(pageWait);
  const [captcha, setCaptcha] = useState("");
  const [amountBuy, setAmountBuy] = useState(1);
  const [verifyCaptcha, setVerifyCaptcha] = useState("");
  const [errorText, setErrorText] = useState("");

  if (time === endtime) {
    // error too many loops
    setlosecount();
    // setTimeout(() => {
    //   // show lose result page and endgame
    //   // setPage(pageWait);
    //   // setendgame();
    // }, 1000);
  }

  function checkResult() {
    // check available no. of item against item in stock (instock)
    // check captcha
    // if both pass setwincount then show success page
    if (
      parseFloat(amountBuy) <= parseFloat(instock) &&
      verifyCaptcha === captcha
    ) {
      setwincount();
    } else {
      setErrorText("Invalid amount or captcha is wrong");
    }
  }

  console.log("captcha", captcha);

  return (
    <>
      <div className="columns">
        <div className="column is-one-third">
          <figure className="image">
            <img src="./assets/bcimage.jpg" alt="" />
          </figure>
        </div>
        <div className="column is-one-third">
          <h2 className="subtitle">Product: {product}</h2>
          <h2 className="subtitle">In stock: {instock}</h2>
          <h2 className="subtitle">
            Start at: {moment(starttime).format("D MMM YYYY HH:mm:ss")}
          </h2>
        </div>
        <div className="column is-one-third">
          <div className="title is-4">
            Starts in
            <span className="title is-1" style={{ marginLeft: ".3em" }}>
              {hoursDiff}
            </span>
            h
            <span className="title is-1" style={{ marginLeft: ".3em" }}>
              {minutesDiff}
            </span>
            m
            <span className="title is-1" style={{ marginLeft: ".3em" }}>
              {secondsDiff}
            </span>
            s
          </div>
          {page === pageWait ? (
            <div>
              <button
                onClick={() => {
                  // setwincount();
                  setPage(pageBuy);
                  // setendgame();
                }}
                className="button is-info"
                disabled={time < starttime}
              >
                Buy now
              </button>
            </div>
          ) : null}
          <div className={`modal ${page === pageBuy ? "is-active" : ""}`}>
            <div className="modal-background" />
            <div className="modal-card">
              <header className="modal-card-head">
                <p className="modal-card-title"> </p>
                <button
                  className="delete"
                  aria-label="close"
                  onClick={e => setPage(pageWait)}
                />
              </header>

              <section className="modal-card-body">
                <div className="columns is-mobile">
                  <div className="column is-two-thirds">
                    <div className="field is-horizontal">
                      <div className="field-label">
                        <label className="label">Available</label>
                      </div>
                      <div className="control">
                        <input className="input" disabled value={instock} />
                      </div>
                    </div>
                    <div className="field is-horizontal">
                      <div className="field-label">
                        <label className="label">Amount</label>
                      </div>
                      <div className="control">
                        <input
                          className="input"
                          value={amountBuy}
                          onChange={e => {
                            setErrorText("");
                            setAmountBuy(e.target.value);
                          }}
                          type="number"
                          placeholder="Amount to buy"
                        />
                      </div>
                    </div>
                    <div className="field is-horizontal">
                      <div className="field-label">
                        <label className="label">Captcha</label>
                      </div>
                      <div className="control">
                        <input
                          className="input"
                          value={verifyCaptcha}
                          onChange={e => {
                            setErrorText("");
                            setVerifyCaptcha(e.target.value);
                          }}
                          type="text"
                          placeholder="Verify captcha -->"
                        />
                      </div>
                    </div>
                  </div>
                  <div className="column is-one-third">
                    <div
                      className="has-text-centered"
                      style={{
                        backgroundImage: "url(./assets/captcha-bg.jpg)"
                      }}
                    >
                      <div
                        className="container is-hidden-mobile"
                        style={{ marginTop: 95 }}
                      />
                      {/* <RCG result={setCaptcha} height={50}  paddingTop={1} fontSize={16} lenght={3}/> */}
                      <Captcha
                        height={30}
                        width={100}
                        length={5}
                        refreshkey={starttime}
                        result={setCaptcha}
                      />
                    </div>
                  </div>
                </div>
              </section>
              <footer className="card-footer">
                <button
                  className="card-footer-item is-info"
                  onClick={e => {
                    checkResult();
                  }}
                >
                  {errorText === "" ? "Confirm" : errorText}
                </button>
              </footer>
              {/* <button
              className="modal-close is-large"
              aria-label="close"
              onClick={e => setPage(pageWait)}
            /> */}
              {/* </div> */}
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
