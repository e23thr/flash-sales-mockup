import React, { useState, useEffect } from "react";
import faker from "faker";
import random from "../lib/random";
import moment from "moment";

import Setup from "./setup";
import Buypage from "./buypage";
import ResultPage from "./resultpage";

const statePause = "PAUSE";
const stateWaitForBuy = "WAIT-BUY-ENABLE";
// const stateBuyEnable = "BUY-ENABLE";
// const stateBuyEnd = "BUY-END";
// const stateShowResult = "SHOW-RESULT";
const stateResultWin = "WIN-RESULT";
const stateResultLose = "LOSE-RESULT";
const formatCompare = moment.HTML5_FMT.DATETIME_LOCAL_SECONDS;

const defaultTimeout = 5;

export default function Main() {
  let Timer = null;
  const m = moment().subtract(1, "h");
  // configuration
  const [timeBeforeBuy, setTimeBeforeBuy] = useState(defaultTimeout);
  const [timeDuringBuy, setTimeDuringBuy] = useState(defaultTimeout * 4);

  // internal data
  // const [counter, setCounter] = useState(moment().seconds()); // counter for timer

  const [thisTime, setThisTime] = useState(moment().format(formatCompare));
  const [buyTime, setBuyTime] = useState(m.format(formatCompare));
  const [endTime, setEndTime] = useState(m.format(formatCompare));

  const [appState, setAppState] = useState(statePause); // state for page default to statePause
  const [win, setWin] = useState(0); // win count
  const [lose, setLose] = useState(0); // lose count
  const [product, setProduct] = useState(""); // random product name
  const [inStock, setInStock] = useState(0); // random stock on hand

  function beginBuyPage() {
    setBuyTime(
      moment()
        .add(timeBeforeBuy, "seconds")
        .format(formatCompare)
    );
    setEndTime(
      moment()
        .add(timeBeforeBuy + timeDuringBuy, "seconds")
        .format(formatCompare)
    );
    setProduct(faker.commerce.product());
    setInStock(random(100, 1000));
    setAppState(stateWaitForBuy);
  }

  useEffect(() => {
    Timer = setInterval(() => {
      setThisTime(moment().format(formatCompare));
    }, 1000);
    // beginBuyPage(); // for dev mode to skip config page
    return () => {
      clearInterval(Timer);
    };
  }, []);

  useEffect(() => {
    setProduct(faker.commerce.product());
    setInStock(random(100, 1000));
  }, []);

  const renderPage = () => {
    switch (appState) {
      case stateWaitForBuy:
        return (
          <Buypage
            product={product}
            time={thisTime}
            starttime={buyTime}
            endtime={endTime}
            instock={inStock}
            setwincount={() => {
              setTimeout(() => {
                setWin(win + 1);
                setEndTime(
                  moment()
                    .subtract(30, "seconds")
                    .format(formatCompare)
                );
                setAppState(stateResultWin);
              }, 200);
              // setWin(win+1);
            }}
            setlosecount={() => {
              setTimeout(() => {
                setLose(lose + 1);
                setAppState(stateResultLose);
              }, 200);
              // setLose(lose+1);
            }}
            setendgame={() => {
              beginBuyPage();
            }}
          />
        );
      case stateResultWin:
        return (
          <ResultPage
            count={win}
            text="Win"
            className="is-success"
            startover={() => {
              // setAppState(stateWaitForBuy);
              beginBuyPage();
            }}
          />
        );
      case stateResultLose:
        return (
          <ResultPage
            count={lose}
            text="Lose"
            className="is-danger"
            startover={() => {
              // setAppState(stateWaitForBuy);
              beginBuyPage();
            }}
          />
        );
      // case stateShowResult:
      //   return <div>result</div>;
      // case stateBuyEnd:
      //   setTimeout(() => {
      //     beginBuyPage();
      //   });
      //   return <div />;
      default:
        return (
          <Setup
            timebeforebuy={timeBeforeBuy}
            timeduringbuy={timeDuringBuy}
            setconfig={(tbb, tdb) => {
              setTimeBeforeBuy(tbb);
              setTimeDuringBuy(tdb);
              beginBuyPage();
            }}
          />
        );
    }
  };

  return (
    <section className="hero is-primary is-fullheight">
      <div className="hero-body">
        <div className="container">{renderPage()}</div>
      </div>
      <div className="hero-foot">
        <nav className="level is-mobile">
          <div className="level-item" />
          <div className="level-item has-text-centered">
            <div>
              <p className="heading">WIN</p>
              <p className="title">{win}</p>
            </div>
          </div>
          <div className="level-item has-text-centered">
            <div>
              <p className="heading">LOSE</p>
              <p className="title">{lose}</p>
            </div>
          </div>
          <div className="level-item" />
        </nav>
      </div>
    </section>
  );
}
